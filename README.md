# Android test task

## What has to be done

Develop an app that displays a list of transactions, transaction details, and a profile.

1. Splash screen
    - Standard Android splash screen
2. Bottom nav bar
    - Tab #1 - List of transactions
        - header with general information (account № + total balance)
        - displays transaction list with min info
        - clicking on the transaction will open the transaction details in the bottom sheet: date + amount (additional information can be added if needed)
    - Tab #2 - Profile
        - shows the user details (optional)
        - clickable support phone number

## Tech stack

We are using the following stack in our project, and would like to see it in the test task

1. UI
    - We have a clean project built from scratch with jetpack compose.
2. Ssynchronous work
    - we use Coroutines + Flow
3. DI
    - We work with Hilt (Dagger)
4. Architecture
    - we use MVVM + State

## What we will look at when evaluating a job

1. Architecture
    - We are a young startup, we are multi-modular and we try to invest in architecture because we will need to grow and change rapidly
2. Code readability
    - It's cool if your code is understandable to all your colleagues
3. Stability and smooth running
4. Knowledge of jetpack compose
    - know how to customize UI (themes, fonts, colors)
    - we want to see that you know how to work with recomposition
    - Side Effects is not a new word for you
    - you know how to navigate
    - understand how to write ui/unit tests
    
## Additional information

1. Design to your liking
2. Network layer can be implemented using open API or use Mochi
